package com.greatLearning.assignment.week2;
import java.util.*;
import java.util.Collections;
public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
	 Set<String> l = new TreeSet<String>();
     ArrayList<String> a = new ArrayList<String>();
     System.out.print("{");
     for (Employee s : employees) {
          a.add(s.getCity());
          l.add(s.getCity());
       }
      for (String s : l) {
         System.out.print(s + "=" + Collections.frequency(a, s) + " ");
   }
     System.out.print("}");
}
public void monthlySalary(ArrayList<Employee>employees) 
{
     System.out.print("{");
     for (Employee e : employees) {
           System.out.print(e.getId() + "=" + (float)(e.getSalary() / 12) + " ");
     }
     System.out.print("}");
     

}

}
